#include <bits/stdc++.h>
using namespace std;
stack<int> stackToHoldLeastHeightFromFirstRow;
int numberOfRowsOfFighters;
int heightsOfFighters[10][100000];
int numberOfFightersInEachRow[100000];
int findindexOfNextMinimumHeight(int row, int minimumHeight)
{
    int mid, low = 0, high = numberOfFightersInEachRow[row];
    high--;
    while (low < high)
    {
        mid = (low + high) / 2;
        if (heightsOfFighters[row][mid] > minimumHeight)
            high = mid;
        else
            low = mid + 1;
    }
    if (heightsOfFighters[row][low] > minimumHeight)
        return low;
    return -1;
}
bool canHarryUseHisWand()
{
	int minHeight = stackToHoldLeastHeightFromFirstRow.top();

	for (int row = 1; row < numberOfRowsOfFighters; row++)
	{
		int position = findindexOfNextMinimumHeight(row, minHeight);
		if (position == -1)
			return false;
		else
			minHeight = heightsOfFighters[row][position];
	}
	return true;
}
void removeFighter(int operatedRow)
{
	operatedRow--;
	numberOfFightersInEachRow[operatedRow]--;
	if (operatedRow == 0)
	{
		stackToHoldLeastHeightFromFirstRow.pop();
	}
}
void addFighter(int operatedRow, int heightToBeAdded)
{
	operatedRow--;
	heightsOfFighters[operatedRow][numberOfFightersInEachRow[operatedRow]] = heightToBeAdded;
	numberOfFightersInEachRow[operatedRow]++;
	if (operatedRow == 0)
	{
		if (heightToBeAdded < stackToHoldLeastHeightFromFirstRow.top())
		{
			stackToHoldLeastHeightFromFirstRow.push(heightToBeAdded);
		}
		else
		{
			stackToHoldLeastHeightFromFirstRow.push(stackToHoldLeastHeightFromFirstRow.top());
		}
	}
}
void decisionByMonk()
{
	if (canHarryUseHisWand())
		cout << "YES\n";
	else
		cout << "NO\n";
}
void operations(int typeOfOperation)
{
	int operatedRow, heightToBeAdded;
	if (typeOfOperation == 1)
	{
		cin >> operatedRow >> heightToBeAdded;
		addFighter(operatedRow, heightToBeAdded);
	}
	else if (typeOfOperation == 0)
	{
		cin >> operatedRow;
		removeFighter(operatedRow);
	}
	else
	{
		decisionByMonk();
	}
}
int main()
{
	int numberOfFightersInTheRow, numberOfOperations;
	int typeOfOperation;
	cin >> numberOfRowsOfFighters;
	for (int row = 0; row < numberOfRowsOfFighters; row++)
	{
		cin >> numberOfFightersInTheRow;
		numberOfFightersInEachRow[row] = numberOfFightersInTheRow;
		for (int positionOfFighter = 0; positionOfFighter < numberOfFightersInTheRow; positionOfFighter++)
		{
			cin >> heightsOfFighters[row][positionOfFighter];
		}
	}
	stackToHoldLeastHeightFromFirstRow.push(heightsOfFighters[0][0]);
	for (int fighterPosition = 1; fighterPosition < numberOfFightersInEachRow[0]; fighterPosition++)
	{
		if (heightsOfFighters[0][fighterPosition] < stackToHoldLeastHeightFromFirstRow.top())
		{
			stackToHoldLeastHeightFromFirstRow.push(heightsOfFighters[0][fighterPosition]);
		}
		else
		{
			stackToHoldLeastHeightFromFirstRow.push(stackToHoldLeastHeightFromFirstRow.top());
		}
	}
	cin >> numberOfOperations;
	for (int index = 0; index < numberOfOperations; index++)
	{
		cin >> typeOfOperation;
		operations(typeOfOperation);
	}
	return 0;
}