def isArmstrongNumber(number):
    # Initializing Sum and Number of Digits
    sumOfDigits = 0
    numberOfDigits = 0
    # Calculating Number of individual digits
    valueHolder = number
    while valueHolder > 0:
        numberOfDigits = numberOfDigits + 1
        valueHolder = valueHolder // 10
    # Finding Armstrong Number
    valueHolder = number
    for value in range(1, valueHolder + 1):
        lastDigit = valueHolder % 10
        sumOfDigits = sumOfDigits + ( lastDigit ** numberOfDigits)
        valueHolder //= 10
    return  sumOfDigits
# End of Function
# User Input
number = int(input("\nPlease Enter the Number to Check for Armstrong: "))
if (number == isArmstrongNumber(number)):
    print("\n %d is Armstrong Number.\n" % number)
else:
    print("\n %d is Not a Armstrong Number.\n" % number)