import java.util.Arrays;
/**
 * Java program to find largest and smallest number from an array.
 */
public class MinimumAndMaximumNumber{
 
    public static void main(String args[]) {
        findMinimumAndMaximumNumber(new int[]{-20, 34, 21, -87, 92,
                             Integer.MAX_VALUE});
        findMinimumAndMaximumNumber(new int[]{10, Integer.MIN_VALUE, -2});
        findMinimumAndMaximumNumber(new int[]{Integer.MAX_VALUE, 40,
                             Integer.MAX_VALUE});
        findMinimumAndMaximumNumber(new int[]{1, -1, 0});
    }
 
    public static void findMinimumAndMaximumNumber(int[] numbers) {
        int largestNumber = Integer.MIN_VALUE;
        int smallestNumber = Integer.MAX_VALUE;
        for (int number : numbers) {
            if (number > largestNumber) {
                largestNumber = number;
            } else if (number < smallestNumber) {
                smallestNumber = number;
            }
        }
 
        System.out.println("Given integer array : " + Arrays.toString(numbers));
        System.out.println("Largest number in array is : " + largestNumber);
        System.out.println("Smallest number in array is : " + smallestNumber);
    }
}