import sys 
import socket    
import argparse           
 
def Main():
        try:
           socketObject = socket.socket(socket.AF_INET, socket.SOCK_STREAM)  
        except socket.error, e:
           print "Error creating socket: %s" % e
           sys.exit(1)
        port = 3000      
        try:      
          socketObject.connect(('127.0.0.1', port)) 
        except socket.gaierror, e:
          print "Address-related error connecting to server: %s" % e
          sys.exit(1)
        except socket.error, e:
          print "Connection error: %s" % e
          sys.exit(1)
        dataToBeSent = raw_input("Command: ")
        while dataToBeSent != 'quit':
                socketObject.send(dataToBeSent)
                dataReceived = socketObject.recv(1024)
                print "data received from server: \n " + dataReceived
                dataToBeSent = raw_input("Command: ")
        socketObject.close()
        
if __name__ == "__main__":
        Main()              
