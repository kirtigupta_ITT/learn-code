export interface ClientData {
      key: string;
      value: string;
      ttl: number;
}