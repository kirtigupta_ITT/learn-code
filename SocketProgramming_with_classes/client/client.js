"use strict";
exports.__esModule = true;
var net = require("net");
var readline = require("readline");
var Config_1 = require("../Config");
var Client = /** @class */ (function () {
    function Client() {
        this.client = new net.Socket();
        this.readline = readline;
        var readLine = this.readline.createInterface({ input: process.stdin });
        this.connectToServer(readLine);
    }
    Client.prototype.connectToServer = function (readLine) {
        try {
            this.client.connect(Config_1.Config.port, Config_1.Config.host, function () {
                console.log('Connected to server');
            });
            this.readInput(readLine);
            this.getDataFromServer();
            this.closeReadLine(readLine);
        }
        catch (exception) {
            console.log("EXCEPTION", exception);
        }
    };
    Client.prototype.readInput = function (readLine) {
        var _this = this;
        readLine.on('line', function (line) {
            _this.client.write(line + "\n");
        });
    };
    Client.prototype.getDataFromServer = function () {
        this.client.on('data', function (data) {
            console.log((data.toString('utf-8')));
        });
    };
    Client.prototype.closeReadLine = function (readLine) {
        var _this = this;
        readLine.on('close', function () {
            _this.client.end();
        });
    };
    return Client;
}());
exports.Client = Client;
var client = new Client();
