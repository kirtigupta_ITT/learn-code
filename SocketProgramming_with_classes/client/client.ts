import * as net from 'net';
import * as readline from 'readline';
import {Config} from '../Config';

export class Client {
        private client = new net.Socket();
        private readline = readline;

        constructor() {
                let readLine = this.readline.createInterface({input: process.stdin});
                this.connectToServer(readLine);
        }

        private connectToServer(readLine) {
                try {
                        this.client.connect(Config.port, Config.host, () => {
                                console.log('Connected to server');
                        });
                        this.readInput(readLine);
                        this.getDataFromServer();
                        this.closeReadLine(readLine);
                } catch (exception) {
                        console.log("EXCEPTION", exception);
                }
        }

        private readInput(readLine) {
                readLine.on('line', (line) => {
                        this.client.write(`${line}\n`);
                });
        }

        private getDataFromServer() {
                this.client.on('data', (data) => {
                        console.log((data.toString('utf-8')));
                });
        }

        private closeReadLine(readLine) {
                readLine.on('close', () => {
                        this.client.end();
                });
        }
}

let client = new Client();