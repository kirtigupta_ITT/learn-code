import {IOperation} from "./IOperation";
import {Hset} from "./Hset";
import {Hget} from "./Hget";
import {Set} from "./Set";
import {Get} from "./Get";

export class CommandFactory {
    private clientData: any;
    private fieldMap: any;

    constructor(clientData, fieldMap) {
        this.clientData = clientData;
        this.fieldMap = fieldMap;
    }

    public getStorageType(storageType: string): IOperation {
        if (storageType.toUpperCase() === "HSET") {
            return new Hset(this.fieldMap);
        }
        if (storageType.toUpperCase() === "HGET") {
            return new Hget(this.fieldMap);
        }
        else if (storageType.toUpperCase() === "SET") {
            return new Set(this.clientData);
        }
        else if (storageType.toUpperCase() === "GET") {
            return new Get(this.clientData);
        }
        else {
            return null;
        }
    }
}