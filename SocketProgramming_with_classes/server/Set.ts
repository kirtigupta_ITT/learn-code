import {IOperation} from "./IOperation";
import {Config} from '../Config';

export class Set implements IOperation {

    private clientData;

    constructor(clientData) {
        this.clientData = clientData;
    }

    public execute(input: Array<string>) {
        if (input.length !== 4) {
            return Config.invalid_arguments;
        }
        else {
            for (let index = 0; index < this.clientData.length; index++) {
                if (this.clientData[index].key === input[1]) {
                    this.clientData[index].value = input[2];
                }
            }
            this.clientData.push({
                "key": input[1],
                "value": input[2],
                "ttl": Math.floor(Date.now() / 1000) + Number(input[3])
            });
            return "OK";
        }
}
}