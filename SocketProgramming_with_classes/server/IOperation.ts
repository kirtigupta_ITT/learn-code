export interface IOperation {
    execute(input:Array<string>);
}