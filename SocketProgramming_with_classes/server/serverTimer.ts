import * as net from 'net';
import {Config} from '../Config';
import {ClientData} from '../model';

export class Server {
        private clientData = [];
        private net: any;
        private server;

        constructor() {
                this.server = new net.Server();
                this.createServer();
        }

        public createServer() {
                try {
                        net.createServer().listen(Config.port, Config.host)
                                .on('connection', (socket) => {
                                        for(let i=0;i < 1000000; i++) {
                                                this.clientData.push({
                                                        "key": `${"user"+ i}`,
                                                        "value":`${"kirti"+ i}`,
                                                        "ttl": 100
                                                })
                                        }
                                        console.log("client data-->", this.clientData);
                                        socket.on('data', (data) => {
                                                let input = this.parseInput(data);
                                                socket.write(this.serveClient(input));
                                        });
                                        socket.on('end', () => {
                                                console.log('Closed');
                                        });
                                });
                } catch (exception) {
                        console.log("Error on connection", exception);
                }
        }

        private serveClient(input: Array<string>) {
                if (input[0].toUpperCase() === Config.set) {
                        return (this.setData(input));
                }
                if (input[0].toUpperCase() === Config.get) {
                        return (this.getData(input, this.clientData));
                }
                else {
                        return Config.invalid_command;
                }
        }

        private parseInput(data) {
                let input = data.toString('utf-8').trim().split(" ");
                for (let index = 0; index < input.length; index++) {
                        input[index] = input[index].replace(/["/\n]/g, "");
                }
                return input;
        }

        private setData(input: Array<string>) {
                if (input.length !== 4) {
                        return Config.invalid_arguments;
                }
                else {
                        let ttl =  Number(input[3]) || 100;
                        for (let index = 0; index < this.clientData.length; index++) {
                                if (this.clientData[index].key === input[1]) {
                                        this.clientData[index].value = input[2];
                                }
                        }
                        this.clientData.push({
                                "key": input[1],
                                "value": input[2],
                                "ttl": ttl
                        });
                        let index = this.clientData.length - 1;
                        setTimeout(this.deleteData, ttl* 1000, index, this.clientData);
                        return "OK";
                }
        }

        private getData(input: Array<string>, clientData: any) {
                if (input.length !== 2) {
                        return Config.invalid_arguments;
                }
                else {
                        for (let index = 0; index < this.clientData.length; index++) {
                                if (this.clientData[index].key === input[1]) {
                                                return this.clientData[index].value;
                                } 
                        }
                        return Config.not_found;
                }
        }

        private deleteData(index: number, clientData: Array<ClientData>) {
                        clientData.splice(index, 1);
                        console.log("index", clientData.length);
        }

}

let server = new Server();