"use strict";
exports.__esModule = true;
var net = require("net");
var Config_1 = require("../Config");
var Server = /** @class */ (function () {
    function Server() {
        this.clientData = [];
        this.server = new net.Server();
        this.createServer();
    }
    Server.prototype.createServer = function () {
        var _this = this;
        try {
            net.createServer().listen(Config_1.Config.port, Config_1.Config.host)
                .on('connection', function (socket) {
                socket.on('data', function (data) {
                    var input = _this.parseInput(data);
                    socket.write(_this.serveClient(input));
                });
                socket.on('end', function () {
                    console.log('Closed');
                });
            });
        }
        catch (exception) {
            console.log("EXCEPTION", exception);
        }
    };
    Server.prototype.serveClient = function (input) {
        if (input[0].toUpperCase() === Config_1.Config.set) {
            return (this.setData(input));
        }
        if (input[0].toUpperCase() === Config_1.Config.get) {
            return (this.getData(input));
        }
        else {
            return Config_1.Config.invalid_command;
        }
    };
    Server.prototype.parseInput = function (data) {
        var input = data.toString('utf-8').trim().split(" ");
        for (var index = 0; index < input.length; index++) {
            input[index] = input[index].replace(/["/\n]/g, "");
        }
        return input;
    };
    Server.prototype.setData = function (input) {
        if (input.length !== 3) {
            return Config_1.Config.invalid_arguments;
        }
        else {
            for (var index = 0; index < this.clientData.length; index++) {
                if (this.clientData[index].key === input[1]) {
                    this.clientData[index].value = input[2];
                }
            }
            this.clientData.push({
                "key": input[1],
                "value": input[2]
            });
            return "OK";
        }
    };
    Server.prototype.getData = function (input) {
        if (input.length !== 2) {
            return Config_1.Config.invalid_arguments;
        }
        else {
            for (var index = 0; index < this.clientData.length; index++) {
                if (this.clientData[index].key === input[1]) {
                    return this.clientData[index].value;
                }
            }
            return Config_1.Config.not_found;
        }
    };
    return Server;
}());
exports.Server = Server;
var server = new Server();
