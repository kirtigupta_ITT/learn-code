import * as net from 'net';
import {Config} from '../Config';
import {CommandFactory} from './CommadFactory';

export class Server {
        private clientData = [];
        private fieldsData = new Map();
        private net: any;
        private server;

        storageFactory: CommandFactory = new CommandFactory(this.clientData, this.fieldsData);

        constructor() {
                this.server = new net.Server();
                this.createServer();
        }

        public createServer() {
                try {
                        net.createServer().listen(Config.port, Config.host)
                                .on('connection', (socket) => {
                                        socket.on('data', (data) => {
                                                let input = this.parseInput(data);
                                                socket.write(this.serveClient(input));
                                        });
                                        socket.on('end', () => {
                                                console.log('Closed');
                                        });
                                });
                } catch (exception) {
                        console.log("Error on connection", exception);
                }
        }

        private serveClient(input: Array<string>) {
                let command = this.storageFactory.getStorageType(input[0]);
                if(!command) {
                        return Config.invalid_command;
                }
                return command.execute(input);
        }

        private parseInput(data) {
                let input = data.toString('utf-8').trim().split(" ");
                for (let index = 0; index < input.length; index++) {
                        input[index] = input[index].replace(/["/\n]/g, "");
                }
                return input;
        }

}

let server = new Server();