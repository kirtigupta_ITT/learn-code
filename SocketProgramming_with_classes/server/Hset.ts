import {IOperation} from "./IOperation";
import {Config} from '../Config';

export class Hset implements IOperation {
    private fieldMap;

    constructor(fieldMap: any) {
        this.fieldMap = fieldMap;
    }

    public execute(input: Array<string>): string {
        if (input.length < 4 && ((input.length % 2) != 0)) {
            return Config.invalid_arguments;
        }
        else {
            if (input.length % 2 === 0) {
                let mapName = this.getNameOfStoredMap(input[1]);
                mapName.set(input[2], input[3]);
                return Config.ok;
            }
            else {
                return Config.invalid_arguments;
            }

        }
    }

    private getNameOfStoredMap(hashMapName: string) {
        let mapName;
        if (!this.fieldMap.has(hashMapName)) {
            mapName = new Map();
        }
        else {
            mapName = this.fieldMap.get(hashMapName);
        }
        this.fieldMap.set(hashMapName, mapName);
        return mapName;
    }
}