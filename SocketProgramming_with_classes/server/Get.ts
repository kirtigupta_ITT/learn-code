import {IOperation} from "./IOperation";
import {Config} from '../Config';
import {ClientData} from '../model';

export class Get implements IOperation {

    private clientData;

    constructor(clientData) {
        this.clientData = clientData;
    }

    public execute(input: Array<string>) {
        if (input.length !== 2) {
            return Config.invalid_arguments;
        }
        else {
            for (let index = 0; index < this.clientData.length; index++) {
                if (this.clientData[index].key === input[1]) {
                    let isExpired = this.isDataExpired(this.clientData[index]);
                    if (isExpired) {
                        this.deleteData(index);
                        return Config.not_found;
                    } else {
                        return this.clientData[index].value;
                    }
                }
            }
            return Config.not_found;
        }
    }

    private isDataExpired(input: ClientData) {
        if (input.ttl < Math.floor(Date.now() / 1000)) {
            return true;
        } else {
            return false;
        }
    }
    private deleteData(index: number) {
        this.clientData.splice(index, 1);
    }
}