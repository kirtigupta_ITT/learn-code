import {IOperation} from "./IOperation";
import {Config} from '../Config';

export class Hget implements IOperation {
    private fieldMap;

    constructor(fieldMap: any) {
        this.fieldMap = fieldMap;
    }

    public execute(input: Array<string>): string {
        if (input.length !== 3) {
            return Config.invalid_arguments;
        }
        else {
            let mapName = this.fieldMap.get(input[1]);
            let value = mapName.get(input[2]);
            if (value) {
                return value;
            } else {
                return Config.not_found;
            }
        }
    }
}