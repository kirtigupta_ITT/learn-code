export const Config = {
        set: "SET",
        get: "GET",
        hset: "HSET",
        hget: "HGET",
        lpop: "LPOP",
        lpush: "LPUSH",
        port: 3000,
        host: "localhost",
        invalid_arguments: "Invalid number of arguments",
        not_found: "Not Found",
        invalid_command: "Invalid command",
        empty_list: "Empty set or list",
        ok: "OK"
}