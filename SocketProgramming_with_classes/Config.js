"use strict";
exports.__esModule = true;
exports.Config = {
    set: "SET",
    get: "GET",
    hset: "HSET",
    hget: "HGET",
    port: 3000,
    host: "localhost",
    invalid_arguments: "Invalid number of arguments",
    not_found: "Not Found",
    invalid_command: "Invalid command"
};
