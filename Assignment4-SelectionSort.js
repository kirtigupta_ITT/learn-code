function sortNumbers(numbers) {
for (let i= 0; i < numbers.length; i++) {
let indexOfMinimumValue = i;
    for(let j = i + 1; j < numbers.length; j++) {
        if (numbers[j] < numbers[indexOfMinimumValue]) {
            indexOfMinimumValue = j;
        }
    }
   if(i !== indexOfMinimumValue) {
      [numbers[i], numbers[indexOfMinimumValue]] = [numbers[indexOfMinimumValue], numbers[i]];
   }
  }
  return numbers;
}
