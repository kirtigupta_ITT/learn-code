const net = require('net');
let constant = require("../constant");
let clientData = [];

const server = net.createServer((socket) => {
        socket.on('data', (data) => {
                let input = parseInput(data);
                if (input[0].toUpperCase() === constant.set) {
                        let value = setData(input);
                        socket.write(value);
                }

                if (input[0].toUpperCase() === constant.get) {
                        let value = getData(input);
                        socket.write(value);
                }
        });

        socket.on('end', () => {
                console.log('Closed');
        });
});

function parseInput(data) {
        let input = data.toString('utf-8').split(" ");
        for (let index = 0; index < input.length; index++) {
                input[index] = input[index].replace(/["/\n]/g, "");
        }
        return input;
}

function setData(input) {
        if (input.length !== 3) {
                return constant.invalid_arguments;
        }
        else {
                clientData.push({
                        "key": input[1],
                        "value": input[2]
                });
                return "OK";
        }
}

function getData(input) {
        if (input.length !== 2) {
                return constant.invalid_arguments;
        }
        else {
                for (let index = 0; index < clientData.length; index++) {
                        if (clientData[index].key === input[1]) {
                                return clientData[index].value;
                        }
                }
                return constant.not_found;
        }
}

server.listen(constant.port);