const net = require('net');
const readline = require('readline');
let constant = require("../constant");

const client = new net.Socket();
client.connect(constant.port, process.argv[2], () => {
        console.log('Connected to server');
});

client.on('data', (data) => {
        console.log((data.toString('utf-8')));
});

const readLine = readline.createInterface({ input: process.stdin });

readLine.on('line', (line) => {
        client.write(`${line}\n`);
});

readLine.on('close', () => {
        client.end();
});