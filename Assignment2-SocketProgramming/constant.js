module.exports = {
        set: "SET",
        get: "GET",
        port: 3000,
        invalid_arguments: "Invalid number of arguments",
        not_found: "Not Found"
}