def isGuessedNumberWithinRange(guessedNumber):
    if guessedNumber.isdigit() and 1<= int(guessedNumber) <=100:
        return True
    else:
        return False
def main():
    numberGenerated=random.randint(1,100)
    isNumberGuessed=False
    guessedNumber=input("Guess a number between 1 and 100:")
    numberOfGuesses=0
    while not isNumberGuessed:
        if not isGuessedNumberWithinRange(numberGuessed):
            guessedNumber=input("I wont count this one Please enter a number between 1 to 100")
            continue
        else:
            numberOfGuesses+=1
            guessedNumber=int(guessedNumber)
        if numberGuessed<numberGenerated:
            guessedNumber=input("Too low. Guess again")
        elif guessedNumber> numberGenerated
            guessedNumber=input("Too High. Guess again")
        else:
            print("You guessed it in",numberOfGuesses,"guesses!")
            isNumberGuessed=True
main()