import random
def findNumberOnDice(maxRange):
    numberOnDice=random.randint(1, maxRange)
    return  numberOnDice
def main():
    maxRange=6
    rollTheDice=True
    while rollTheDice:
        repeatRolling=input("Ready to roll? Enter Q to Quit")
        if repeatRolling.lower() !="q":
            numberOnDice=findNumberOnDice(maxRange)
            print("You have rolled a",numberOnDice)
        else:
            rollTheDice=False