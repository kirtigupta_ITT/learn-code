var express = require('express');
var app = express();
var path = require('path');
var bodyParser = require("body-parser");
var cors = require('cors');

app.use(cors())
app.use(bodyParser.json())
var jsonParser = bodyParser.json();
let data = [];
app.post('/signup', jsonParser, function (req, res) {
        data.push((req.body));
        let message = { "message": "Registered Successfully" }
        res.send(message);

});

app.get('/signin', function (req, res) {
        if (req.query.email === data[0].email && req.query.password === data[0].password) {
                console.log(data[0]);
                res.status(200).send(data[0]);
        }
        else {
                res.status(401).send(null);
        }
});

var server = app.listen(3001, function () {
        console.log('Node server is running..');
});