import {Component, OnInit} from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {DataService} from '../data.service';


@Component({
        selector: 'app-login',
        templateUrl: './login.component.html',
        styleUrls: ['./login.component.css']
})
export class LoginComponent {
        firstName: string;
        lastName: string;
        email: string;
        password: string;
        confirmPassword: string;
        gender: string;
        dateOfBirth: string;

        constructor(private dataService: DataService, private http: HttpClient) {
        }

        loginUser(loginValues) {
                this.email = loginValues.userName;
                this.password = loginValues.password;
                this.dataService.getUserData(this.email, this.password)
                        .subscribe((response: any) => {
                                alert('Login Successful');
                        }, (err: HttpErrorResponse) => {
                                alert('Please enter registered username/Password');
                        });
        }
}
