import {TestBed} from '@angular/core/testing';
import {HttpClientModule} from '@angular/common/http';
import {DataService} from './data.service';

describe('DataService', () => {
        beforeEach(() => TestBed.configureTestingModule({
                imports: [HttpClientModule],
                providers: [DataService]
        }));

        it('should be created', () => {
                const service: DataService = TestBed.get(DataService);
                expect(service).toBeTruthy();
        });
        it('be able to retrieve user data from the API via GET', () => {
                const service: DataService = TestBed.get(DataService);
                const mockTestData: any = {
                        firstName: 'Kirti',
                        lastName: 'Gupta',
                        email: 'kirti.gupta@intimetec.com',
                        password: 'kirti@123',
                        dob: '2020-10-28',
                        gender: 'female'
                };
                service.getUserData('kirti.gupta@intimetec.com', 'kirti@123')
                        .subscribe((userData: any) => {
                                expect(userData.email).toEqual(mockTestData.email);
                        });
        });

        it('Should test the post data and response from post request', () => {
                const service: DataService = TestBed.get(DataService);
                const mockTestData = {
                        firstName: 'Kirti',
                        lastName: 'Gupta',
                        email: 'kirti.gupta@intimetec.com',
                        password: 'kirti@123',
                        dob: '2020-10-28',
                        gender: 'female'
                };
                service.storeUserData(mockTestData)
                        .subscribe((response: any) => {
                                expect(response.message).toEqual('Registered Successfully');
                        });
        });
});