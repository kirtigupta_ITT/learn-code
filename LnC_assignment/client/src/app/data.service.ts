import {HttpClient, HttpParams} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {map} from 'rxjs/operators';


@Injectable({
        providedIn: 'root',
})
export class DataService {

        constructor(private http: HttpClient) {}
        storeUserData(userData: any) {
                const url = 'http://localhost:3001/signup';
                return this.http.post(url, userData);
        }
        getUserData(email: string, password: string) {
                const params = new HttpParams().set('email', email)
                        .set('password', password);
                const url = 'http://localhost:3001/signin';
                return this.http.get(url, {params})
                        .pipe(map(
                                (response: Response) => {
                                        return response;
                                }
                        ));
        }
}