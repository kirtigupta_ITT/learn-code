import {Component, OnInit} from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {DataService} from '../data.service';

@Component({
        selector: 'app-register',
        templateUrl: './register.component.html',
        styleUrls: ['./register.component.css']
})
export class RegisterComponent {

        firstName: string;
        lastName: string;
        email: string;
        password: string;
        confirmPassword: string;
        gender: string;
        dateOfBirth: string;

        constructor(private dataService: DataService, private http: HttpClient) {
        }

        registerUser(value) {
                this.firstName = value.firstName;
                this.lastName = value.lastName;
                this.email = value.emailId;
                this.password = value.password;
                this.gender = value.gender;
                this.dateOfBirth = value.DOB;
                const userData = {
                        firstName: this.firstName,
                        lastName: this.lastName,
                        gender: this.gender,
                        dob: this.dateOfBirth,
                        email: this.email,
                        password: this.password,
                };
                this.dataService.storeUserData(userData)
                        .subscribe(response => {
                                console.log(response);
                                alert('Registered Successfully');
                        }, (err: HttpErrorResponse) => {
                                console.log(err);
                        });
        }

}
